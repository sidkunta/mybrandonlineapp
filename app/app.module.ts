import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { AppRoutingModule } from './app.routing';
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular';
import { NativeScriptUIDataFormModule } from 'nativescript-ui-dataform/angular';
import { AppComponent } from './app.component';
import { ItemService } from './item/item.service';
// import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
import { ItemsComponent } from './item/items.component';
import { ItemDetailComponent } from './item/item-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DirectoryComponent } from './directory/directory.component';
import { MemberDetailComponent } from './directory/member-detail.component';
import { RaterequestComponent } from './raterequest/raterequest.component';
import { MyshipmentsComponent } from './myshipments/myshipments.component';
import { MyshipmentDetailComponent } from './myshipments/myshipment-detail/myshipment-detail.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationDetailComponent } from './notifications/notification-detail/notification-detail.component';
import { ChatComponent } from './chat/chat.component';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { DirectoryService } from './services/directory.service';
import { LoginComponent } from './login/login.component';
import { AuthService } from './services/auth.services';
import { DataSharingService } from './services/dataSharing.service';
import { Feedback } from 'nativescript-feedback';
import { AuthGuard } from './services/auth-guard.service';
import { NewsService } from './services/news.service';
import { MyShipmentAirService } from './services/myshipmentsAir.service';
import { NotificationsService } from './services/notifications.service';
import { AccordionModule } from 'nativescript-accordion/angular';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';
import { RateRequestService } from './services/rateRequest.service';
import { RrDashboardComponent } from './raterequest/rr-dashboard/rr-dashboard.component';
import { RrdetailComponent } from './raterequest/rrdetail/rrdetail.component';
import { AgentDirectoryComponent } from './directory/agent-directory/agent-directory.component';
import { AgentdirectoryDetailsComponent } from './directory/agentdirectory-details/agentdirectory-details.component';

@NgModule({
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptUIListViewModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIDataFormModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        AccordionModule,
    ],
    declarations: [
        AppComponent,
        ItemsComponent,
        ItemDetailComponent,
        DashboardComponent,
        DirectoryComponent,
        AgentDirectoryComponent,
        MemberDetailComponent,
        AgentdirectoryDetailsComponent,
        RaterequestComponent,
        RrDashboardComponent,
        RrdetailComponent,
        MyshipmentsComponent,
        MyshipmentDetailComponent,
        ChatComponent,
        NotificationsComponent,
        NotificationDetailComponent,
        LoginComponent
    ],
    providers: [
        ItemService,
        DirectoryService,
        AuthService,
        DataSharingService,
        Feedback,
        AuthGuard,
        NewsService,
        MyShipmentAirService,
        NotificationsService,
        RateRequestService
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {}
