import { Component, OnInit } from '@angular/core';
import * as frameModule from 'ui/frame';
import * as ApplicationSettings from 'application-settings';

@Component({
    moduleId: module.id,
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
    notificationCount: number;
    constructor() {}
    ngOnInit() {
        this.notificationCount = ApplicationSettings.getNumber('notificationCount', 0);
    }
    goBack() {
        frameModule.topmost().goBack();
    }
}
