import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { Routes } from '@angular/router';

import { ItemsComponent } from './item/items.component';
import { ItemDetailComponent } from './item/item-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DirectoryComponent } from './directory/directory.component';
import { MemberDetailComponent } from './directory/member-detail.component';
import { RaterequestComponent } from './raterequest/raterequest.component';
import { ChatComponent } from './chat/chat.component';
import { MyshipmentsComponent } from './myshipments/myshipments.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { LoginComponent } from './login/login.component';
// import { AuthGuard } from './services/auth-guard.service';
import { MyshipmentDetailComponent } from './myshipments/myshipment-detail/myshipment-detail.component';
import { NotificationDetailComponent } from './notifications/notification-detail/notification-detail.component';
import { RrDashboardComponent } from './raterequest/rr-dashboard/rr-dashboard.component';
import { RrdetailComponent } from './raterequest/rrdetail/rrdetail.component';
import { AgentDirectoryComponent } from './directory/agent-directory/agent-directory.component';
import { AgentdirectoryDetailsComponent } from './directory/agentdirectory-details/agentdirectory-details.component';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    // { path: '', component: LoginComponent },
    { path: 'items', component: ItemsComponent },
    { path: 'item/:id', component: ItemDetailComponent },
    { path: 'dashboard', component: DashboardComponent },

    { path: 'directory', component: DirectoryComponent },
    { path: 'agentdirectory', component: AgentDirectoryComponent},
    { path: 'member/:AgentID', component: MemberDetailComponent },
    { path: 'agentmember/:AgentID', component: AgentdirectoryDetailsComponent},
    
    { path: 'raterequest', component: RaterequestComponent },
    { path: 'rrdashboard', component: RrDashboardComponent},
    { path: 'rrdetail/:RateRequestId', component:RrdetailComponent},

    { path: 'chat', component: ChatComponent },

    { path: 'myshipments', component: MyshipmentsComponent },
    {
        path: 'myshipmentDetail/:type/:JobMasterNo',
        component: MyshipmentDetailComponent
    },

    { path: 'notifications', component: NotificationsComponent },
    { path: 'notification-detail', component: NotificationDetailComponent },
    
    { path: 'login', component: LoginComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
