import { Time } from '@angular/common';

export class UserModel {
    constructor(
        public LastLogin: string = '',
        public UserDetails: UserDetail,
        public AgentDetails: AgentDetail,
        public UserPreferences: UserPreference,
        public DateFormat: Date,
        public TimeFormat: Time,
        public Controls: Control,
        public Networks: string = '',
        public ContactEmail: string = '',
        public Version: version
    ) {}
}

export class UserDetail {
    constructor(
        public ContactID: number,
        public AgentID: number,
        public FirstName: string = '',
        public LastName: string = '',
        public Email: string = '',
        public JobTitle: string = '',
        public ContactNumber: number,
        public IsActivated: boolean,
        public OperatedBy: string = '',
        public AddressBookID: number,
        public Permissions: string[] = [],
        public ContactType: string = '',
        public UCTReferer: string = '',
        public UCTIsCaptcha: boolean,
        public UATReferer: string = '',
        public UATIsCaptcha: boolean,
        public UserName: string = ''
    ) {}
}

export class AgentDetail {
    constructor(
        public AgentID: number,
        public AgentName: string = '',
        public AgentBranchName: string = '',
        public Services: string[] = [],
        public Networks: string[] = [],
        public Permissions: string[] = []
    ) {}
}

export class UserPreference {
    constructor(
        public AboutUs: string = '',
        public MOTDSubject: string = '',
        public MOTDMessage: string = '',
        public Language: string = '',
        public Currency: string = '',
        public UnitSystem: string = '',
        public TimeZone: timezone
    ) {}
}

export class timezone {
    constructor(
        public TheID: number,
        public KeyIdentifier: string = '',
        public KeyValue: string = '',
        public KeyDescription: string = '',
        public IsDefault: boolean
    ) {}
}

export class Control {
    constructor(public MaxRFQ: number) {}
}

export class version {
    constructor(
        public APIVersion: string = '',
        public UIVersion: string = '',
        public MobileVersion: string = ''
    ) {}
}
