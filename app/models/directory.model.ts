export class Directory {
    constructor(
        public MSDBID: number,
        public AgentID: number,
        public AgentName: string = '',
        public RegistrationType: string,
        public AccountNumber: number,
        public IATACode: string = '',
        public Address1: string = '',
        public Address2: string = '',
        public Place: string = '',
        public StateProvince: string = '',
        public City: CodeName = new CodeName(),
        public Country: CodeName = new CodeName(),
        public Zipcode: number,
        public Website: string = '',
        public Email: string = '',
        public Phone: number,
        public Fax: number,
        public IsAirWayBill: boolean,
        public Location: LatLong = new LatLong(),
        public Networks: Network[] = [],
        public Services: CodeName[] = [],
        public Permissions: string[] = [],
        public Quotes: Quote[] = [],
        public Controls: Control[] = [],
        public AboutUs: string = ''
    ) {}
}

export class CodeName {
    constructor(public Code: string = '', public Name: string = '') {}
}
export class LatLong {
    constructor(public lat: string = '', public long: string = '') {}
}

export class Network {
    constructor(
        public Code: string = '',
        public Name: string = '',
        public ValidFrom: string,
        public ValidTo: string
    ) {}
}

export class Quote {
    constructor(
        public RFQResponseRatio: string = '',
        public QuoteResponseRatio: string = ''
    ) {}
}

export class Control {
    constructor(public MaxRFQ: number) {}
}
