export class News {
    constructor(
        public BroadcastNotificationID: number,
        public Message: string = '',
        public Subject: string = '',
        public CreatedOn: Date
    ) {}
}
