import { CodeName } from "./myShipmentAir.model";

export class MyShipmentDetail{
    constructor(
     public WinID : string = '',
     public JobMasterNo : string = '',
     public JobStatus : string = '',
     public ReferenceID : string = '',
     public TransportMode : string = '',
     public Origin : string = '',
     public Destination : string = '',
     public MasterNumber : string = '',
     public AWBNumber : string = '',
     public MBLNumber : string = '',
     public IsMasterShared : boolean,
     public Carrier : string = '',
     public Containers : container[],
     public Visibility: visibility[],
     public Remarks : string = ''
    ){}
}

export class container{
    constructor (
        public Number : string = '',
        public Type : string = ''
    ){}
}

export class visibility {
    constructor (
    public AddressBookID : number,
    public Name : string = '',
    public City : CodeName,
    public Country : CodeName,
    ){}
}