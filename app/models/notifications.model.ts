export class Notifications {
    constructor(
        public EventID: number,
        public Status: status,
        public From: detail,
        public To: detail
    ) {}
}

export class status {
    constructor(
        public Type: string = '',
        public Code: string = '',
        public Name: string = '',
        public DateTime: Date,
        public Data: data
    ) {}
}

export class data {
    constructor(
        public JobMasterID: number,
        public TransportMode: string = '',
        public JobMasterNo: string = '',
        public ReferenceID: string = '',
        public ReferenceNo: string = '',
        public StatusID: number,
        public StatusCode: string = '',
        public StatusDescription: string = '',
        public StatusDateTime: Date
    ) {}
}

export class detail {
    constructor(
        public AgentID: number,
        public AgentName: string = '',
        public Country: string = '',
        public City: string = ''
    ) {}
}
