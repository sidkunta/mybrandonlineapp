export class RateRequestForm {
    constructor(
        public TransportMode: string = '',
        public From: string = '',
        public To: string = '',
        public ShipmentDate: string = '',
        public ShipmentDetails: string = '',
        public IncoTerms: string = '',
        public RateRequestId? : number,
        public Status? : string,
        public CreatedOn? : string,
        public AddressBookName? : string
    ) {}
}

