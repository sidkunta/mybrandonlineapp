export class MyShipmentAir {
    constructor(
        public TransactionType: string = '',
        public MasterID: number,
        public MasterNo: number,
        public JobMasterNo: number,
        public IsMasterShared: boolean,
        public Status: string = '',
        public TransportMode: string = '',
        public Origin: origin,
        public UniqueReferenceID: string = '',
        public Destination: destination,
        public ETD: string = '',
        public ETA: string = '',
        public Houses: string = '',
        public LatestEventStatus: latestEventStatus,
        public NotificationPreference: notificationPreference
    ) {}
}
export class CodeName {
    constructor(public Code: string = '', public Name: string = '') {}
}
export class origin {
    constructor(
        public CountryCode: string = '',
        public Code: string = '',
        public Name: string = ''
    ) {}
}
export class destination {
    constructor(
        public CountryCode: string = '',
        public Code: string = '',
        public Name: string = ''
    ) {}
}
export class latestEventStatus {
    constructor(
        public Event: event,
        public Location: CodeName,
        public DateTime: string = ''
    ) {}
}

export class event {
    constructor(public Code: string = '', public Name: string = '') {}
}
export class notificationPreference {
    constructor(
        public EmailNotification: boolean,
        public PushNotification: boolean
    ) {}
}
