import {
    Component,
    OnInit,
    // ViewEncapsulation,
    Input,
    // SimpleChanges,
    // EventEmitter,
    // Output
} from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { Directory } from '../models/directory.model';
import { DirectoryService } from '../services/directory.service';
// import { Router } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import * as dialogs from 'ui/dialogs';
import * as ApplicationSettings from 'application-settings';

@Component({
    moduleId: module.id,
    selector: 'app-directory',
    templateUrl: './directory.component.html',
    styleUrls: ['./directory.component.scss']
})
export class DirectoryComponent implements OnInit {
    constructor(
        private dashboardResultService: DirectoryService,
        // private router: Router,
        private routerExtensions: RouterExtensions
    ) {}

    pager: any = {};
    pagedItems: any[];
    directoryResults: Directory[];
    directoryInfo: any;
    _filter: string;
    authToken: string;
    errorMsg: string;
    isLoading = true;
    notificationCount: number;

    @Input()
    set filter(value: string) {
        this._filter = value;
        this.getDirectoryResults();
    }

    getDirectoryResults() {
        console.log('entered service call');
        this.dashboardResultService.getDirectoryResults().subscribe(
            dashboardResults => {
                // console.log('Results: ' + dashboardResults);
                this.pagedItems = dashboardResults;
                this.isLoading = false;
            },
            error => {
                dialogs
                    .alert({
                        title: 'Service failed',
                        message: error,
                        okButtonText: 'Close'
                    })
                    .then(() => {
                        this.errorMsg = error;
                    });
            }
        );
    }

    ngOnInit() {
        this.isLoading = true;
        this.getDirectoryResults();
        this.notificationCount = ApplicationSettings.getNumber('notificationCount', 0);
        console.log('directory page initiated');
    }

    goBack() {
        this.routerExtensions.back();
    }
}
