import { Component, OnInit, Input } from '@angular/core';
import { Directory } from '../../models/directory.model';
import { DirectoryService } from '../../services/directory.service';
import { RouterExtensions } from 'nativescript-angular/router';
import * as dialogs from 'ui/dialogs';
import * as ApplicationSettings from 'application-settings';


@Component({
  moduleId: module.id,
  selector: 'app-agent-directory',
  templateUrl: './agent-directory.component.html',
  styleUrls: ['./agent-directory.component.scss']
})
export class AgentDirectoryComponent implements OnInit {

  constructor(
    private dashboardResultService: DirectoryService,
    // private router: Router,
    private routerExtensions: RouterExtensions
) {}

pager: any = {};
pagedItems: any[];
directoryResults: Directory[];
directoryInfo: any;
_filter: string;
authToken: string;
errorMsg: string;
isLoading = true;
notificationCount: number;


@Input()
set filter(value: string) {
    this._filter = value;
    this.getAgentDirectoryResults();
}

getAgentDirectoryResults() {
    console.log('entered service call');
    this.dashboardResultService.getAgentDirectoryResults().subscribe(
        dashboardResults => {
            // console.log('Results: ' + dashboardResults);
            this.pagedItems = dashboardResults;
            this.isLoading = false;
        },
        error => {
            dialogs
                .alert({
                    title: 'Service failed',
                    message: error,
                    okButtonText: 'Close'
                })
                .then(() => {
                    this.errorMsg = error;
                });
        }
    );
}

ngOnInit() {
    this.isLoading = true;
    this.getAgentDirectoryResults();
    this.notificationCount = ApplicationSettings.getNumber('notificationCount', 0);
    console.log('directory page initiated');
}

goBack() {
    this.routerExtensions.back();
}
}