import {
  Component,
  OnInit,
  // ViewEncapsulation,
  Input,
  // SimpleChanges,
  // EventEmitter,
  // Output
} from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { Directory } from '../../models/directory.model';
import { DirectoryService } from '../../services/directory.service';
import {
  // Router,
  ActivatedRoute
} from '@angular/router';
// import * as dialogs from 'ui/dialogs';
import { RouterExtensions } from 'nativescript-angular/router';
import * as ApplicationSettings from 'application-settings';

@Component({
  moduleId: module.id,
  selector: 'app-agentdirectory-details',
  templateUrl: './agentdirectory-details.component.html',
  styleUrls: ['./agentdirectory-details.component.scss']
})
export class AgentdirectoryDetailsComponent implements OnInit {

  constructor(
    private memberDetailService: DirectoryService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    // private router: Router
) {}

pager: any = {};
pagedItems: any[];
memberResults: Directory;
directoryInfo: any;
_filter: string;
authToken: string;
errorMsg: string;
isLoading = true;
public AgentID: number;
notificationCount: number;

@Input()
set filter(value: string) {
    this._filter = value;
    this.getAgentMemberDetail(1);
}

getAgentMemberDetail(AgentID) {
    // console.log('entered service call', AgentID);
    this.memberDetailService.getAgentMemberDetail(AgentID).subscribe(
        memberDetails => {
            // console.log('Resposnse: ', memberDetails);
            this.pagedItems = memberDetails;
            this.isLoading = false;
        },
        () => {
            // console.log(`Error: ${error.status} Message: ${error.error}`);
        }
    );
}

ngOnInit() {
    this.isLoading = true;
    this.notificationCount = ApplicationSettings.getNumber('notificationCount', 0);
    this.route.data.subscribe(() => {
        this.AgentID = this.route.snapshot.params.AgentID;
        this.getAgentMemberDetail(this.AgentID);
        // console.log('Member Detail page initiated', data);
    });
}
goBack() {
    this.routerExtensions.back();
}
}