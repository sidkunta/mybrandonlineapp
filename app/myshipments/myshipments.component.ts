import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { Router } from '@angular/router'; 
import { SearchBar } from 'ui/search-bar';
import { MyShipmentAir } from '../models/myShipmentAir.model';
import { MyShipmentAirService } from '../services/myshipmentsAir.service';
import * as ApplicationSettings from 'application-settings';
import * as dialogs from 'ui/dialogs';

@Component({
    moduleId: module.id,
    selector: 'app-myshipments',
    templateUrl: './myshipments.component.html',
    styleUrls: ['./myshipments.component.scss']
})
export class MyshipmentsComponent implements OnInit {
    public tabSelectedIndex: number;
    searchPhrase;
    _filter: string;
    pagedItemsAir: any[];
    myShipmentAirResults: MyShipmentAir[];
    pagedItemsRoad: any[];
    myShipmentRoadResults: MyShipmentAir[];
    filteredItemsAir: any;
    filteredItemsSea: any;
    filteredItemsRoad: any;
    pagedItemsSea: any[];
    myShipmentSeaResults: MyShipmentAir[];
    airJobCount = 0;
    seaJobCount = 0;
    roadJobCount = 0;
    itemsLoaded = 0;
    isLoading = true;
    searchBy;
    searchByDisp;
    
    showDetails = false;

    notificationCount: number;
    
    toggle() {
        this.showDetails = !this.showDetails;
    }
    // public onSubmit(args) {
    //     const searchBar = <SearchBar>args.object;
    //     alert('You are searching for ' + searchBar.text);
    // }

    // public onTextChanged(args) {
    //     const searchBar = <SearchBar>args.object;
    //     console.log('SearchBar text changed! New value: ' + searchBar.text);
    // }
    constructor(
        private routerExtensions: RouterExtensions,
        private myShipmentAirService: MyShipmentAirService,
        private router: Router
    ) {
        switch(ApplicationSettings.getString('defaultVisibility', 'air')) {
            case 'air': this.tabSelectedIndex = 0; break;
            case 'sea': this.tabSelectedIndex = 1; break;
            case 'road': this.tabSelectedIndex = 2; break;
            default: break;
        }

        this.itemsLoaded = 0;
        this.searchByDisp = 'Master Number';
        this.searchBy = 'MasterNo';
    }

    ngOnInit() {
        this.getMyShipmentAir();
        this.getMyShipmentRoad();
        this.getMyShipmentSea();
        this.notificationCount = ApplicationSettings.getNumber('notificationCount', 0);
    }

    onSearchByTap() {
        dialogs.action('Choose a filtering method', 'Cancel', [
            'Master ID',
            'Master Number',
            'JobMaster Number',
            'Unique Reference ID',
        ]).then((value) => {
            if (value !== 'Cancel') {
                switch (value) {
                    case 'Master ID': this.searchBy = 'MasterID'; break;
                    case 'Master Number': this.searchBy = 'MasterNo'; break;
                    case 'JobMaster Number': this.searchBy = 'JobMasterNo'; break;
                    case 'Unique Reference ID': this.searchBy = 'UniqueReferenceID'; break;
                    default: break;
                }
                this.searchByDisp = value;
            }
        });
    }

    getMyShipmentAir() {
        console.log('My Shipment Air Available');
        this.myShipmentAirService
            .getMyshipmentAir()
            .subscribe(myShipmentAirResults => {
                // console.log('My Shipment Air Results:' + myShipmentAirResults);
                this.pagedItemsAir = myShipmentAirResults;
                console.log(JSON.stringify(this.pagedItemsAir[0]));
                this.filteredItemsAir = [...this.pagedItemsAir];
                this.airJobCount = this.pagedItemsAir.length;
                this.itemsLoaded += 1;
                if (this.itemsLoaded === 3) {
                    this.isLoading = false;
                }
            });
    }

    getMyShipmentRoad() {
        console.log('My Shipment Road Available');
        this.myShipmentAirService
            .getMyshipmentRoad()
            .subscribe(myShipmentRoadResults => {
                // console.log(
                //     'My Shipment R  oad Results:' + myShipmentRoadResults
                // );
                this.pagedItemsRoad = myShipmentRoadResults;
                this.filteredItemsRoad = [...this.pagedItemsRoad];
                this.roadJobCount = this.pagedItemsRoad.length;
                this.itemsLoaded += 1;
                if (this.itemsLoaded === 3) {
                    this.isLoading = false;
                }
            });
    }

    getMyShipmentSea() {
        console.log('My Shipment Sea Available');
        this.myShipmentAirService
            .getMyshipmentSea()
            .subscribe(myShipmentSeaResults => {
                // console.log('My Shipment Sea Results:' + myShipmentSeaResults);
                this.pagedItemsSea = myShipmentSeaResults;
                this.filteredItemsSea = [...this.pagedItemsSea];
                this.seaJobCount = this.pagedItemsSea.length;
                this.itemsLoaded += 1;
                if (this.itemsLoaded === 3) {
                    this.isLoading = false;
                }
            });
    }

    onSearchTextChangedAir(args) {
        this.filterResults(args, this.pagedItemsAir, 'air');
    }

    onSearchTextChangedSea(args) {
        this.filterResults(args, this.pagedItemsSea, 'sea');
    }

    onSearchTextChangedRoad(args) {
        this.filterResults(args, this.pagedItemsRoad, 'road');
    }

    filterResults(args, pagedItems, type) {
        const searchBar = <SearchBar>args.object;

        console.log('search by', this.searchBy);
        const filtered = [... pagedItems.filter((item) => {
            if (!searchBar.text || searchBar.text === '') {
                return true;
            }

            if (item[this.searchBy]) {
                console.log('enter');

                return item[this.searchBy].toLowerCase().indexOf(searchBar.text.toLowerCase()) !== -1;
            } else {
                console.log('never');

                return false;
            }
        })];

        switch(type) {
            case 'air': 
                this.filteredItemsAir = [...filtered];
                break;
            case 'sea': 
                this.filteredItemsSea = [...filtered];
                break;
            case 'road': 
                this.filteredItemsRoad = [...filtered];
                break;
        }
    }

    onSearch(args) {
        const searchBar = <SearchBar>args.object;
        console.log('You are searching for ' + searchBar.text);
    }

    public sBLoaded(args) {
        const searchbar: SearchBar = <SearchBar>args.object;
        if (searchbar.android) {
            searchbar.android.clearFocus();
        }
    }

    goBack() {
        this.routerExtensions.back();
    }

    gotoAirShipmentDetail(args) {
        console.log(this.pagedItemsAir[args.index].JobMasterNo);
        this.router.navigate(['/myshipmentDetail', 'air', this.pagedItemsAir[args.index].JobMasterNo]);
    }

    gotoSeaShipmentDetail(args) {
        console.log(this.pagedItemsSea[args.index].JobMasterNo);
        this.router.navigate(['/myshipmentDetail', 'sea', this.pagedItemsSea[args.index].JobMasterNo]);
    }

    gotoRoadShipmentDetail(args) {
        console.log(this.pagedItemsRoad[args.index].JobMasterNo);
        this.router.navigate(['/myshipmentDetail', 'road', this.pagedItemsRoad[args.index].JobMasterNo]);
    }
}
