import { Component, OnInit, Input } from '@angular/core';
import { Directory } from '../../models/directory.model';
import { MyShipmentDetail } from '../../models/myShipmentDetail.model';
import { MyShipmentAirService } from '../../services/myshipmentsAir.service';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { getString, getNumber } from 'tns-core-modules/application-settings';
import { NotificationsService } from '../../services/notifications.service';
import { Feedback } from 'nativescript-feedback';
@Component({
    moduleId: module.id,
    selector: 'app-myshipment-detail',
    templateUrl: './myshipment-detail.component.html',
    styleUrls: ['./myshipment-detail.component.scss'],
    providers: [Feedback]
})
export class MyshipmentDetailComponent implements OnInit {

    constructor(private myShipmentDetailService: MyShipmentAirService,
        private route: ActivatedRoute,
        private notificationsService: NotificationsService,
        private routerExtensions: RouterExtensions,
        private feedback: Feedback
    ) {
        this.LoggedInUser = JSON.parse(getString('userDetails', '{}'));
    }

    pager: any = {}
    pagedItems: any[];
    memberResults: Directory;
    shipmentDetail: MyShipmentDetail;
    directoryInfo: any;
    _filter: string;
    authToken: string;
    errorMsg: string;
    isLoading = false;
    notification;
    notificationData;
    locationCodes;
    locationSet: any;
    JobMasterNo: string;
    filteredItems: any;
    LoggedInUser: any;
    type: string;
    date = Date();
    notificationCount: number;
    @Input() set filter(value: string) {
        this._filter = value;
        this.getShipmentDetailAir(1);
    }

    ngOnInit() {
        this.isLoading = true;
        this.notificationCount = getNumber('notificationCount', 0);
        this.route.data
            .subscribe((_data: any) => {
                this.JobMasterNo = this.route.snapshot.paramMap.get('JobMasterNo');
                this.type = this.route.snapshot.paramMap.get('type');
                this.getShipmentDetailAir(this.JobMasterNo);
                this.getNotificationDetail();
                switch (this.type) {
                    case 'air': this.getShipmentDetailAir(this.JobMasterNo); break;
                    case 'sea': this.getShipmentDetailSea(this.JobMasterNo); break;
                    case 'road': this.getShipmentDetailRoad(this.JobMasterNo); break;
                    default: break;
                }
                console.log('Shipment Air detail page initiated');
            });
    }

    getShipmentDetailAir(JobMasterNo) {
        console.log('entered service call', JobMasterNo);
        this.myShipmentDetailService
            .getShipmentDetailAir(JobMasterNo)
            .subscribe(
                memberDetails => {
                    console.log("got the details");
                    console.log(JSON.stringify(memberDetails));
                    this.pagedItems = memberDetails;
                    this.isLoading = false;
                },
                (error) => {
                    console.log(`Error: ${error.status} Message: ${JSON.stringify(error.error)}`);
                }
            );
    }

    getShipmentDetailSea(JobMasterNo) {
        // console.log('entered service call', JobMasterNo);
        this.myShipmentDetailService
            .getShipmentDetailSea(JobMasterNo)
            .subscribe(
                memberDetails => {
                    // console.log('Resposnse: ', memberDetails);
                    this.pagedItems = memberDetails;
                    this.isLoading = false;
                },
                () => {
                    // console.log(`Error: ${error.status} Message: ${error.error}`);
                }
            );
    }

    getShipmentDetailRoad(JobMasterNo) {
        // console.log('entered service call', JobMasterNo);
        this.myShipmentDetailService
            .getShipmentDetailRoad(JobMasterNo)
            .subscribe(
                memberDetails => {
                    // console.log('Resposnse: ', memberDetails);
                    this.pagedItems = memberDetails;
                    this.isLoading = false;
                },
                () => {
                    // console.log(`Error: ${error.status} Message: ${error.error}`);
                }
            );
    }

    goBack() {
        this.routerExtensions.back();
    }
    
    getNotificationDetail() {
        if (this.LoggedInUser) {
            this.isLoading = true;
            this.notificationsService
                .getNotificationDetail(
                    this.JobMasterNo
                )
                .subscribe(
                    data => {
                        this.isLoading = false;
                        this.notificationData = data;
                        this.parseUniqueLocationCodesAndSeparateIntoArray(data);
                    },
                    () => {
                        this.isLoading = false;
                        // (error);
                        this.feedback.show({
                            message: 'Fetching notifications detail failed!'
                        });
                    }
                );
        }
    }

    parseUniqueLocationCodesAndSeparateIntoArray(data) {
        const locationCodeSet = new Set();
        this.locationSet = {};

        for (const statusUpdate of data.StatusUpdates) {
            if (
                statusUpdate.Source === 'Airline' &&
                statusUpdate.Location.Code
            ) {
                locationCodeSet.add(statusUpdate.Location.Code);
            } else if (
                statusUpdate.Source === 'OceanLiner' &&
                statusUpdate.Containers[0].ContainerNumber
            ) {
                locationCodeSet.add(statusUpdate.Containers[0].ContainerNumber);
            }
        }

        this.locationCodes = Array.from(locationCodeSet);

        for (let i = 0; i < this.locationCodes.length; i++) {
            this.locationSet[this.locationCodes[i]] = [];
            for (const statusUpdate of data.StatusUpdates) {
                if (
                    statusUpdate.Source !== 'OceanLiner' &&
                    this.locationCodes[i] === statusUpdate.Location.Code
                ) {
                    this.locationSet[this.locationCodes[i]].push(statusUpdate);
                } else if (
                    statusUpdate.Source === 'OceanLiner' &&
                    this.locationCodes[i] ===
                    statusUpdate.Containers[0].ContainerNumber
                ) {
                    this.locationSet[this.locationCodes[i]].push(statusUpdate);
                }
            }
            this.locationCodes[i] = {
                id: this.locationCodes[i],
                name:
                    data.StatusUpdates[0].Location.Name +
                    ` (${this.locationCodes[i]})`,
                isOpen: false
            };
        }

        // console.log(this.locationCodes);
    }
}
