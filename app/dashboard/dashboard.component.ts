import {
    Component,
    ViewChild,
    OnInit,
    Input,
    // AfterViewInit,
    ChangeDetectorRef,
    AfterViewInit,
    ElementRef
} from '@angular/core';
// import { Page } from 'ui/page';
import { Switch } from 'ui/switch';
// import { ActionItem } from 'ui/action-bar';
// import { Observable } from 'data/observable';
import { setTimeout } from 'timer';
// import { ScrollEventData } from 'ui/scroll-view';
import {
    RadSideDrawerComponent,
    // SideDrawerType
} from 'nativescript-ui-sidedrawer/angular';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { News } from '../models/news.model';
// import { LoginComponent } from '../login/login.component';
import { NewsService } from '../services/news.service';
import { RouterExtensions } from 'nativescript-angular';
import * as ApplicationSettings from 'application-settings';
import { Feedback } from 'nativescript-feedback';
import { NotificationsService } from '../services/notifications.service';
import { Label } from 'tns-core-modules/ui/label';
import * as elementRegistryModule from 'nativescript-angular/element-registry';
import { AuthService } from '../services/auth.services';
elementRegistryModule.registerElement(
    'CardView',
    () => require('nativescript-cardview').CardView
);

declare var android: any;

@Component({
    moduleId: module.id,
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers: [Feedback]
})
export class DashboardComponent implements OnInit, AfterViewInit {
    private _mainContentText: string;
    public firstSwitchState = 'Notification OFF';
    public secondSwitchState = 'ON';
    _filter: string;
    public userDetails: any;
    newsItem;
    notificationsData: any;
    notificationCount: number;
    isLoading = false;
    news: any[];
    newsResults: News[];
    defaultVisible;
    @ViewChild('marqueeEl') marqueeRef: ElementRef;
    marqueeLabel: Label;
    public status = 'not scrolling';
    isAgent = false;

    constructor(
        private _changeDetectionRef: ChangeDetectorRef,
        private authService: AuthService,
        private router: RouterExtensions,
        private notificationService: NotificationsService,
        private newsService: NewsService,
        private feedback: Feedback
    ) {
        this.defaultVisible = ApplicationSettings.getString('defaultVisibility', 'air');
        this.isAgent = ApplicationSettings.getBoolean('isAgent', false);
    }

    @ViewChild(RadSideDrawerComponent)
    public drawerComponent: RadSideDrawerComponent;
    private drawer: RadSideDrawer;

    @Input()
    set filter(value: string) {
        this._filter = value;
        this.getNews();
    }

    setDefaultVisibility(visibileType) {
        console.log('default visible', visibileType);
        ApplicationSettings.setString('defaultVisibility', visibileType);
        this.defaultVisible = visibileType;
    }

    getNews() {
        // console.log('News availabele');
        this.newsService.getNews().subscribe(newsResults => {
            // console.log('News Results:' + newsResults);
            // console.log(newsResults);
            this.newsItem = newsResults;
        });
    }

    public onScroll() {
        this.status = 'scrolling';

        setTimeout(() => {
            this.status = 'not scrolling';
        }, 300);

        // console.log('scrollX: ' + args.scrollX);
        // console.log('scrollY: ' + args.scrollY);
    }

    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
        this.marqueeLabel = this.marqueeRef.nativeElement;

        setTimeout(() => {
            if (this.marqueeLabel.android) {
                this.marqueeLabel.android.setEllipsize(android.text.TextUtils.TruncateAt.MARQUEE);
                this.marqueeLabel.android.setMarqueeRepeatLimit(-1);
                this.marqueeLabel.android.setSelected(true);
            }
        }, 1000);
    }

    public onFirstChecked(args) {
        const firstSwitch = <Switch>args.object;
        if (firstSwitch.checked) {
            this.firstSwitchState = 'Notification ON';
        } else {
            this.firstSwitchState = 'Notification OFF';
        }
    }

    ngOnInit() {
        const userDetails = ApplicationSettings.getString('userDetails', '{}');
        if (userDetails === '{}') {
            this.router.navigate(['/login']);
            return;
        }
        this.userDetails = JSON.parse(userDetails);
        // console.log('user details: ');
        // console.dir(this.userDetails);
        this.getNews();
        this.getNotifications();
    }

    get mainContentText() {
        return this._mainContentText;
    }

    set mainContentText(value: string) {
        this._mainContentText = value;
    }

    // Open Drawer method
    public openDrawer() {
        this.drawer.showDrawer();
    }
    logout() {
        console.log('loggin out..');
        this.authService.logout();
        this.router.navigate(['']);
    }
    getNotifications() {
        this.notificationCount = ApplicationSettings.getNumber('notificationCount', 0);
        if (this.userDetails) {
            this.isLoading = true;
            // console.log(
            //     'Getting notifications for ' +
            //         this.userDetails.UserDetails.AgentID
            // );
            this.notificationService
                .getNotifications(this.userDetails.UserDetails.AgentID)
                .subscribe(
                    notificationsResults => {
                        // console.log('Notification Data:' + JSON.stringify(notificationsResults));
                        ApplicationSettings.setString(
                            'notificationsData',
                            JSON.stringify(notificationsResults)
                        );
                        this.notificationsData = notificationsResults;
                        this.notificationCount = notificationsResults.length;
                        ApplicationSettings.setNumber("notificationCount", notificationsResults.length);
                        // console.log(
                        //     'Total notifications: ' + this.notificationCount
                        // );
                        this.isLoading = false;
                    },
                    () => {
                        this.isLoading = false;
                        // console.dir(error);
                        this.feedback.show({
                            message: 'Fetching notifications failed!'
                        });
                    }
                );
        }
    }
}
