// import { Injectable } from '@angular/core';
// import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import { DataSharingService } from './dataSharing.service';
// import 'rxjs/add/operator/do';

// import { Router } from '@angular/router';

// @Injectable()
// export class AuthInterceptor implements HttpInterceptor {
//   constructor(
//     public data: DataSharingService,
//     public router: Router,

//   ) { }
//   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     console.log('token', this.data.getLocalItem('AuthToken'));
//     request = request.clone({
//       setHeaders: {
//         //  AuthToken: `Bearer ${this.data.getLocalItem('AuthToken')}`,
//         //  'Content-Type': `application/json`,
//         // withCredentials: true
//       }
//     });
//     return next.handle(request).do((event: HttpEvent<any>) => {
//     }, (err: any) => {
//       if (err instanceof HttpErrorResponse) {
//         if (err.status === 401) {
//           this.router.navigate(['/login']);
//           return false; // redirect to the login route
//         } else {
//           const message = err.error.Params && err.error.Params[0].Description ? err.error.Params[0].Description : 'Api Error';
//         //   this.growler.growl(message, GrowlerMessageType.Danger);
//         }
//       }
//     });
//   }
// }
