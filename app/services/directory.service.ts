// import { of } from 'rxjs/observable/of';
// import { tap, catchError, map } from 'rxjs/operators';
// import { filter } from 'rxjs/operators/filter';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as ApplicationSettings from 'application-settings';
// import { Directory } from '../models/directory.model';

import {
    HttpClient,
    // HttpHeaders,
    // HttpResponse
} from '@angular/common/http';
// const httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable()
export class DirectoryService {
    constructor(private http: HttpClient) {}

    /** GET Countries from the server */
    getDirectoryResults(): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/AgentList?context=dashboard&$orderby=AgentName%20asc`,
            { withCredentials: true }
        );
    }

    getAgentDirectoryResults(): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/AgentList?context=dashboard&$orderby=RFQResponseRatio%20desc`,
            { withCredentials: true }
        );
    }

    getMemberDetail(AgentID: number): Observable<any> {
        // console.log(
        //     'AgentId',
        //     'http://qat.winwebconnect.com/api/v1/AgentList/' + AgentID
        // );
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/AgentList/` + AgentID,
            { withCredentials: true }
        );
        // .toPromise()
        // .then(response => response)
        // .catch(error => error);
    }

    getAgentMemberDetail(AgentID: number): Observable<any> {
        // console.log(
        //     'AgentId',
        //     'http://qat.winwebconnect.com/api/v1/AgentList/' + AgentID
        // );
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/AgentList/` + AgentID,
            { withCredentials: true }
        );
        // .toPromise()
        // .then(response => response)
        // .catch(error => error);
    }

    getMemberDetailNew(AgentID: number): any {
        // console.log(
        //     'AgentId',
        //     'http://qat.winwebconnect.com/api/v1/AgentList/' + AgentID
        // );
        return this.http
            .get<any>(
                `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/AgentList/` + AgentID,
                { withCredentials: true }
            )
            .subscribe(
                () => {
                    // console.log('Results ', res);
                },
                () => {
                    // console.log('error', error);
                }
            );
    }
}
