// import { of } from 'rxjs/observable/of';
// import { tap, catchError, map } from 'rxjs/operators';
// import { filter } from 'rxjs/operators/filter';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as ApplicationSettings from 'application-settings';

import {
    HttpClient,
    // HttpHeaders,
    // HttpResponse
} from '@angular/common/http';
// const httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable()
export class NotificationsService {
    public currentNotification; // used to store the data of currently selected notification in memory
    // public notificationCount : notificationCount.length;
    constructor(private http: HttpClient) {}
    /** GET News from the server */
    getNotifications(AgentID: number): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/${AgentID}/Notifications`,
            { withCredentials: true }
        );
    }

    getNotificationDetail(jobMasterNumber: string) {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Jobs/${jobMasterNumber}/StatusEvents`,
            { withCredentials: true }
        );
    }

    deleteNotification(
        AgentID: number,
        notificationID: number
    ): Observable<any> {
        return this.http.delete<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/${AgentID}/Notifications/${notificationID}`,
            { withCredentials: true }
        );
    }

    deleteAllNotifications(AgentID: number): Observable<any> {
        return this.http.delete<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/${AgentID}/Notifications/`,
            { withCredentials: true }
        );
    }
}
