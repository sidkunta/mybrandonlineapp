import * as ApplicationSettings from 'application-settings';

export const environment = {
    production: false,
    apiEndPoint: `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com`
  };
  