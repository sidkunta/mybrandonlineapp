// import { of } from 'rxjs/observable/of';
// import { tap, catchError, map } from 'rxjs/operators';
// import { filter } from 'rxjs/operators/filter';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as ApplicationSettings from 'application-settings';
// import { News } from '../models/news.model';

import {
    HttpClient,
    // HttpHeaders,
    // HttpResponse
} from '@angular/common/http';
// const httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable()
export class NewsService {
    constructor(private http: HttpClient) {}
    /** GET News from the server */
    getNews(): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/198804/MobileWhitelabel/MOTD?brand=my_brand`,
            { withCredentials: true }
        );
    }
}
