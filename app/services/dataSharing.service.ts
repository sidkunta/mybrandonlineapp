import { Injectable } from '@angular/core';

@Injectable()
export class DataSharingService {
    public data: any;
    constructor() {}

    getSessionItem<T>(key: string): T {
        if (sessionStorage[key]) {
            return <T>JSON.parse(sessionStorage[key]);
        }
        return null;
    }

    setSessionItem(key: string, item: any) {
        sessionStorage[key] = JSON.stringify(item);
    }

    removeSessionItem(key: string) {
        if (sessionStorage[key]) {
            delete sessionStorage[key];
        }
        return null;
    }

    getLocalItem<T>(key: string): T {
        if (localStorage[key]) {
            return <T>JSON.parse(localStorage[key]);
        }
        return null;
    }

    setLocalItem(key: string, item: any) {
        localStorage[key] = JSON.stringify(item);
    }

    removeLocalItem<T>(key: string): T {
        if (localStorage[key]) {
            return localStorage.removeItem[key];
        }
        return null;
    }
}
