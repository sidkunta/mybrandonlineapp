import { Injectable, Output, EventEmitter } from '@angular/core';
// import {
//     CanActivate,
//     Router,
//     ActivatedRouteSnapshot,
//     RouterStateSnapshot
// } from '@angular/router';
import {
    HttpClient,
    HttpErrorResponse
    // HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';
// import { IUserLogin } from '../models/interfaces';
import { DataSharingService } from './dataSharing.service';
import { UserModel } from '../models/user.model';
import { environment } from './environment';
import * as ApplicationSettings from 'application-settings';

@Injectable()
export class AuthService {
    baseURL: string = environment.apiEndPoint;
    isAuthenticated = false;
    redirectUrl: string;
    @Output() authChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
    private dataSharingService: DataSharingService;
    constructor(private http: HttpClient) {}

    static isLoggedIn(): boolean {
        return ApplicationSettings.getBoolean('authenticated');
    }

    logout() {
        ApplicationSettings.remove('authenticated');
    }

    private userAuthChanged(status: boolean) {
        this.authChanged.emit(status); // Raise changed event
    }

    //   public checkLogin(): Observable<boolean> {
    //     console.log('***** Going to check Login *******');
    //     return new Observable(observer => {
    //       this.http.get<any>(`${this.baseURL}/api/login`, { withCredentials: true }).subscribe(response => {
    //         if (response) {
    //           this.dataSharingService.setSessionItem('User', response);
    //           this.isAuthenticated = true;
    //           this.userAuthChanged(this.isAuthenticated);
    //           observer.next(true);
    //           observer.complete();
    //           this.router.navigate([this.redirectUrl]);
    //         } else {
    //           observer.next(false);
    //           observer.complete();
    //           this.router.navigate(['/login']);
    //         }
    //       },
    //         (err: any) => {
    //           observer.next(false);
    //           observer.complete();
    //           this.router.navigate(['/login']);
    //           console.log(err);
    //         });
    //     });
    //   }

    login(input, isAgent): Observable<boolean> {
        // console.log(input);
        let data;
        if (isAgent) {
            data = {
                UserName: input.Username,
                Password: input.Password
            };
        } else {
            data = {
                Username: input.Username,
                Password: input.Password,
                RememberMe: false,
                Brand: 'my_brand'
            };
        }

        console.log('logging in: ', `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Login${ isAgent ? '' : '/my_brand'}`);

        return this.http
            .post<any>(
                `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Login${ isAgent ? '' : '/my_brand'}`,
                data
            )
            .pipe(
                map(res => {
                    // const authToken: string = res.headers.get('AuthToken');
                    // this.dataSharingService.setSessionItem('User', res);
                    // console.log('userdetails', JSON.stringify(res));
                    this.isAuthenticated = true;
                    ApplicationSettings.setBoolean('authenticated', true);
                    ApplicationSettings.setString(
                        'userDetails',
                        JSON.stringify(res)
                    );
                    this.userAuthChanged(this.isAuthenticated);
                }),
                catchError(this.handleError)
            );
    }
    createUser(res: any): UserModel {
        return new UserModel(
            res.LastLogin,
            res.UserDetails,
            res.AgentDetails,
            res.UserPreferences,
            res.DateFormat,
            res.TimeFormat,
            res.Controls,
            res.Networks,
            res.ContactEmail,
            res.Version
        );
    }

    getActiveUser(): UserModel {
        const loggedUser: UserModel = this.createUser(
            this.dataSharingService.getSessionItem('User')
        );
        return loggedUser ? loggedUser : null;
    }

    private handleError(error: HttpErrorResponse) {
        console.error('server error:', JSON.stringify(error));
        if (error.error instanceof Error) {
            const errMessage = error.error.message;
            return Observable.throw(errMessage);
            // Use the following instead if using lite-server
            // return Observable.throw(err.text() || 'backend server error');
        }
        return Observable.throw(error || 'Server error');
    }

}
