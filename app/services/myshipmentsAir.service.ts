// import { of } from 'rxjs/observable/of';
// import { tap, catchError, map } from 'rxjs/operators';
// import { filter } from 'rxjs/operators/filter';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as ApplicationSettings from 'application-settings';
// import { MyShipmentAir } from '../models/myShipmentAir.model';
// import { MyShipmentAir } from '../models/myShipmentAir.model';

import {
    HttpClient
    // HttpHeaders,
    // HttpResponse
} from '@angular/common/http';
// const httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable()
export class MyShipmentAirService {
    constructor(private http: HttpClient) {}
    /** GET News from the server */
    getMyshipmentAir(): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/198804/MyShipments/AirFreight`,
            { withCredentials: true }
        );
    }

    getShipmentDetailAir(_JobMasterNo: number): Observable<any> {
        // console.log('JobMasterNo', "http://qat.winwebconnect.com/api/v1/JobsLite/JM00035210")
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/JobsLite/JM00035210`,
            { withCredentials: true }
        );
    }

    getMyshipmentRoad(): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/198804/MyShipments/RoadFreight`,
            { withCredentials: true }
        );
    }

    getShipmentDetailRoad(_JobMasterNo: number): Observable<any> {
        // console.log('JobMasterNo', "http://qat.winwebconnect.com/api/v1/JobsLite/JM00035210")
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/JobsLite/JM00035210`,
            { withCredentials: true }
        );
    }

    getMyshipmentSea(): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/198804/MyShipments/SeaFreight`,
            { withCredentials: true }
        );
    }

    getShipmentDetailSea(_JobMasterNo: number): Observable<any> {
        console.log(
            'JobMasterNo',
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/JobsLite/JM00035210`
        );
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/JobsLite/JM00035210`,
            { withCredentials: true }
        );
    }
}
