import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as ApplicationSettings from 'application-settings';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RateRequestService {
    constructor(private http: HttpClient) {}

    /** GET Countries from the server */
    postRateRequest(AgentID : number, data): Observable<any> {
        return this.http.post<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/` + AgentID + `/RateRequest`, data,
            { withCredentials: true }
        );
    }

    getRateRequest(RateRequestId: number): Observable<any> {
        // console.log(
        //     'AgentId',
        //     'http://qat.winwebconnect.com/api/v1/AgentList/' + AgentID
        // );
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/198804/RateRequest/` + RateRequestId,
            { withCredentials: true }
        );
        // .toPromise()
        // .then(response => response)
        // .catch(error => error);
    }

    getRateRequestAll(): Observable<any> {
        return this.http.get<any>(
            `http://${ApplicationSettings.getString('serverEnv', 'qat')}.winwebconnect.com/api/v1/Agents/198804/RateRequest/Dashboard`,
            { withCredentials: true }
        );
    }
}
