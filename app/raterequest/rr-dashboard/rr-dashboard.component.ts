import { Component, OnInit, Input } from '@angular/core';
import { RateRequestForm } from '../../models/raterequestform.model';
import { RouterExtensions } from 'nativescript-angular/router';
import { RateRequestService } from '../../services/rateRequest.service';
// import { Router } from '@angular/router';
import * as dialogs from 'ui/dialogs';
// import * as ApplicationSettings from 'application-settings';
import { getString, getNumber } from 'tns-core-modules/application-settings';
// import { ModalDialogService } from 'nativescript-angular';


@Component({
  moduleId: module.id,
  selector: 'app-rr-dashboard',
  templateUrl: './rr-dashboard.component.html',
  styleUrls: ['./rr-dashboard.component.scss']
})
export class RrDashboardComponent implements OnInit {

  LoggedInUser: any;
  notificationCount: number;
  dashboardResults: RateRequestForm[];
  pagedItems: any[];
  _filter: string;
  authToken: string;
  errorMsg: string;
  isLoading = true;
  public AgentID: number;

  constructor(
    private routerExtensions: RouterExtensions,
    private rateRequestService: RateRequestService,
    // private router: Router
  ) { }

  private _rateRequest: RateRequestForm;

  ngOnInit() {
    this._rateRequest = new RateRequestForm();
    this.getRRDashboardResults();
    this._rateRequest.ShipmentDate = new Date().toISOString().slice(0, 10);
    this.LoggedInUser = JSON.parse(getString('userDetails', '{}'));
    this.notificationCount = getNumber('notificationCount', 0);
   }

   goBack() {
    this.routerExtensions.back();
}

@Input()
set filter(value: string) {
    this._filter = value;
    this.getRRDashboardResults();
}

get raterequest(): RateRequestForm {
    return this._rateRequest;
}

getRRDashboardResults(){
this.rateRequestService.getRateRequestAll().subscribe(
 dashboardResults => {
   this.pagedItems = dashboardResults;
   this.isLoading = false;
 },
 error => {
   dialogs
        .alert({
          title: 'Service failed',
          message: error,
          okButtonText: 'Close'
        })
        .then(() => {
          this.errorMsg = error;
      });
 }

);
}
}
