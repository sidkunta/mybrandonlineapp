import { Component, OnInit } from '@angular/core';
import { RateRequestForm } from '../models/raterequestform.model';
import { getString, getNumber } from 'tns-core-modules/application-settings';
// import { FormsModule } from '@angular/forms';
import { RouterExtensions } from 'nativescript-angular/router';
import { Feedback } from 'nativescript-feedback';
import { RateRequestService } from '../services/rateRequest.service';
import { Router } from '@angular/router';
import * as applicationModule from "tns-core-modules/application";
declare var NSDateFormatter: any;
declare var java: any;

@Component({
    moduleId: module.id,
    selector: 'app-raterequest',
    templateUrl: './raterequest.component.html',
    styleUrls: ['./raterequest.component.scss']
})
export class RaterequestComponent implements OnInit {
    LoggedInUser: any;
    notificationCount: number;
    constructor(
        private routerExtensions: RouterExtensions,
        private feedback: Feedback,
        private rateRequestService: RateRequestService,
        private router: Router
    ) {}

    private _rateRequest: RateRequestForm;

    ngOnInit() {
        this._rateRequest = new RateRequestForm();
        this._rateRequest.ShipmentDate = new Date().toISOString().slice(0, 10);
        this.LoggedInUser = JSON.parse(getString('userDetails', '{}'));
        this.notificationCount = getNumber('notificationCount', 0);
    }

    goBack() {
        this.routerExtensions.back();
    }
    get raterequest(): RateRequestForm {
        return this._rateRequest;
    }

    public onEditorUpdate(args) {
        if (args.propertyName == "ShipmentDate") {
            this.changeDateFormatting(args.editor);
        }
    }

    private changeDateFormatting(editor) {
        if (applicationModule.ios) {
            var dateFormatter = NSDateFormatter.alloc().init();
            dateFormatter.dateFormat = "yyyy-MM-dd";
            editor.dateFormatter = dateFormatter;
        } else {
            var simpleDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US);
            editor.setDateFormat(simpleDateFormat);
        }
    }

    onSubmit() {
        if (
            !(
                this._rateRequest.TransportMode &&
                this._rateRequest.From &&
                this._rateRequest.To &&
                this._rateRequest.ShipmentDate &&
                this._rateRequest.IncoTerms &&
                this._rateRequest.ShipmentDetails
            )
        ) {
            this.feedback.show({
                message: 'Please fill all details'
            });

            return;
        }

        const rateRequest = {
            TransportMode: this._rateRequest.TransportMode,
            From: this._rateRequest.From,
            To: this._rateRequest.To,
            ShipmentDate: this._rateRequest.ShipmentDate,
            IncoTerms: this._rateRequest.IncoTerms,
            ShipmentDetails: this._rateRequest.ShipmentDetails
        };

        this.rateRequestService.postRateRequest(
            this.LoggedInUser.UserDetails.AgentID,
            rateRequest
        ).subscribe(
            (response) => {
                console.log(response);
                this.router.navigate(['/rrdashboard']);
            },
            (error) => {
                console.log(error);
                this.feedback.show({
                    message: 'Unable to send rate request'
                });
            }
        );
    }
}
