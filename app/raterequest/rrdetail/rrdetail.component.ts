import { Component, OnInit, Input} from '@angular/core';
import { RateRequestForm } from '../../models/raterequestform.model';
import { RateRequestService } from '../../services/rateRequest.service';
import { RouterExtensions } from 'nativescript-angular/router';
import {
  // Router,
  ActivatedRoute
}  from '@angular/router';
import * as ApplicationSettings from 'application-settings';


@Component({
  moduleId: module.id,
  selector: 'app-rrdetail',
  templateUrl: './rrdetail.component.html',
  styleUrls: ['./rrdetail.component.scss']
})
export class RrdetailComponent implements OnInit {

  constructor(
    private rateRequestService : RateRequestService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,

  ) { }

  pager: any = {};
  pagedItems: any[];
  rrResults: RateRequestForm;
  rrInfo: any;
  _filter: string;
  authToken: string;
  errorMsg: string;
  isLoading = true;
  public RateRequestId: number;
  notificationCount: number;

  @Input()
  set filter(value: string) {
      this._filter = value;
      this.getRateRequest(1);
  }

  getRateRequest(RateRequestId) {
    // console.log('entered service call', AgentID);
    this.rateRequestService.getRateRequest(RateRequestId).subscribe(
        rrDetails => {
            // console.log('Resposnse: ', memberDetails);
            this.pagedItems = rrDetails;
            this.isLoading = false;
        },
        () => {
            // console.log(`Error: ${error.status} Message: ${error.error}`);
        }
    );
}

ngOnInit() {
  this.isLoading = true;
  this.notificationCount = ApplicationSettings.getNumber('notificationCount', 0);
  this.route.data.subscribe(() => {
      this.RateRequestId = this.route.snapshot.params.RateRequestId;
      this.getRateRequest(this.RateRequestId);
      // console.log('Member Detail page initiated', data);
  });
}
goBack() {
  this.routerExtensions.back();
}

}
