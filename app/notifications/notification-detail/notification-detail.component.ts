import { Component, OnInit } from '@angular/core';
// import { Notifications } from '../models/notifications.model';
import { NotificationsService } from '../../services/notifications.service';
import { Feedback } from 'nativescript-feedback';
import { getString } from 'tns-core-modules/application-settings';
import { topmost } from 'ui/frame';

@Component({
    moduleId: module.id,
    selector: 'app-notification-detail',
    templateUrl: './notification-detail.component.html',
    styleUrls: ['./notification-detail.component.scss'],
    providers: [Feedback]
})
export class NotificationDetailComponent implements OnInit {
    filteredItems: any;
    LoggedInUser: any;
    isLoading = false;
    show404 = false;
    notification;
    notificationData;
    locationCodes;
    locationSet: any;

    constructor(
        private notificationsService: NotificationsService,
        private feedback: Feedback
    ) {
        this.LoggedInUser = JSON.parse(getString('userDetails', '{}'));
    }

    ngOnInit() {
        this.notification = this.notificationsService.currentNotification;
        this.getNotificationDetail();
    }

    getNotificationDetail() {
        if (this.LoggedInUser) {
            this.isLoading = true;

            if (!this.notification.Status || !this.notification.Status.Data || !this.notification.Status.Data.JobMasterNo) {
                this.isLoading = false;
                this.show404 = true;

                return;
            }

            this.notificationsService
                .getNotificationDetail(
                    this.notification.Status.Data.JobMasterNo
                )
                .subscribe(
                    data => {
                        this.isLoading = false;
                        this.notificationData = data;
                        this.parseUniqueLocationCodesAndSeparateIntoArray(data);
                    },
                    () => {
                        this.isLoading = false;
                        // (error);
                        this.feedback.show({
                            message: 'Fetching notifications detail failed!'
                        });
                    }
                );
        }
    }

    parseUniqueLocationCodesAndSeparateIntoArray(data) {
        const locationCodeSet = new Set();
        this.locationSet = {};

        // Create a set of unique location codes
        for (const statusUpdate of data.StatusUpdates) {
            if (
                statusUpdate.Source === 'Airline' &&
                statusUpdate.Location.Code
            ) {
                locationCodeSet.add(statusUpdate.Location.Code);
            } else if (
                statusUpdate.Source === 'OceanLiner' &&
                statusUpdate.Containers[0].ContainerNumber
            ) {
                locationCodeSet.add(statusUpdate.Containers[0].ContainerNumber);
            }
        }

        this.locationCodes = Array.from(locationCodeSet);

        // Create a mapping between unique location codes and its status updates
        for (let i = 0; i < this.locationCodes.length; i++) {
            this.locationSet[this.locationCodes[i]] = [];
            for (const statusUpdate of data.StatusUpdates) {
                if (
                    statusUpdate.Source !== 'OceanLiner' &&
                    this.locationCodes[i] === statusUpdate.Location.Code
                ) {
                    this.locationSet[this.locationCodes[i]].push(statusUpdate);
                } else if (
                    statusUpdate.Source === 'OceanLiner' &&
                    this.locationCodes[i] ===
                        statusUpdate.Containers[0].ContainerNumber
                ) {
                    this.locationSet[this.locationCodes[i]].push(statusUpdate);
                }
            }
            this.locationCodes[i] = {
                id: this.locationCodes[i],
                name:
                    data.StatusUpdates[0].Location.Name +
                    ` (${this.locationCodes[i]})`,
                isOpen: false
            };
        }

        // console.log(this.locationCodes);
    }

    onNavBtnTap() {
        topmost().goBack();
    }
}
