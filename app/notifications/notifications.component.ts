import { Component, OnInit, Input } from '@angular/core';
import { NotificationsService } from '../services/notifications.service';
import { Feedback } from 'nativescript-feedback';
import { getString } from 'tns-core-modules/application-settings';
import { confirm } from 'tns-core-modules/ui/dialogs';
import { ListViewEventData } from 'nativescript-ui-listview';
import { View } from 'tns-core-modules/ui/core/view';
import { SearchBar } from 'ui/search-bar';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss'],
    providers: [Feedback]
})
export class NotificationsComponent implements OnInit {
    public notificationsDetails: any;
    pagedItems: any = [];
    filteredItems: any;
    LoggedInUser: any;
    notifications: any[];
    _filter: string;
    notificationCount: number;
    isLoading = false;
    searchPhrase;
    notificationsResults: Notification[];

    constructor(
        // private authService: AuthService,
        private router: Router,
        private notificationsService: NotificationsService,
        private feedback: Feedback
    ) {
        this.LoggedInUser = JSON.parse(getString('userDetails', '{}'));
    }
    @Input()
    set filter(value: string) {
        this._filter = value;
        this.getNotifications();
    }

    getNotifications() {
        if (this.LoggedInUser) {
            this.isLoading = true;
            this.notificationsService
                .getNotifications(this.LoggedInUser.UserDetails.AgentID)
                .subscribe(
                    notificationsResults => {
                        // console.log('Notification Data:' + JSON.stringify(notificationsResults));
                        this.pagedItems = notificationsResults;
                        this.filteredItems = [...this.pagedItems];
                        this.notificationCount = notificationsResults.length;
                        this.isLoading = false;
                    },
                    () => {
                        this.isLoading = false;
                        // (error);
                        this.feedback.show({
                            message: 'Fetching notifications failed!'
                        });
                    }
                );
        }
    }

    deleteAllNotifications() {
        if (this.LoggedInUser) {
            const options = {
                title: 'Clear  notifications',
                message:
                    'Are you sure you want to delete all notifications data?',
                okButtonText: 'Yes',
                cancelButtonText: 'No',
                neutralButtonText: 'Cancel'
            };

            confirm(options).then((result: boolean) => {
                // console.log(result);
                console.log('Deleting notifications');
                if (result === true) {
                    console.log('Deleting notifications');
                    this.isLoading = true
                    this.notificationsService.deleteAllNotifications(this.LoggedInUser.UserDetails.AgentID)
                        .subscribe(() => {
                            this.getNotifications();
                        }, (error) => {
                            this.isLoading = false;
                            console.dir(error);
                            this.feedback.show({
                                message: 'Deleting notifications failed!'
                            });
                        });
                }
            });
        }
    }

    ngOnInit() {
        this.getNotifications();
    }

    onSwipeCellStarted(args: ListViewEventData) {
        const swipeLimits = args.data.swipeLimits;
        const swipeView = args.object;
        const rightItem = swipeView.getViewById<View>('delete-view');
        swipeLimits.right = rightItem.getMeasuredWidth();
        swipeLimits.left = 0;
        swipeLimits.threshold = rightItem.getMeasuredWidth() / 2;
    }

    delete(args: ListViewEventData) {
        this.isLoading = true;
        const notification = args.object.bindingContext;
        this.notificationsService.deleteNotification(
            this.LoggedInUser.UserDetails.AgentID,
            notification.EventID
        ).subscribe(() => {
            this.getNotifications();
        }, (error) => {
            this.isLoading = false;
            console.dir(error);
            this.feedback.show({
                message: 'Deleting notifications failed!'
            })
        });
    }

    onSearchTextChanged(args) {
        const searchBar = <SearchBar>args.object;

        if (searchBar.text === '') {
            this.filteredItems = [...this.pagedItems];

            return;
        }

        if (this.pagedItems.length) {
            const filtered = this.pagedItems.filter((item) => {
                if (!searchBar.text || searchBar.text === '') {
                    return true;
                }

                if (item.Status && item.Status.Data && item.Status.Data.ReferenceID) {
                    return item.Status.Data.ReferenceID.toLowerCase().indexOf(searchBar.text.toLowerCase()) !== -1;
                } else {
                    return false;
                }
            });

            if (filtered.length) {
                this.filteredItems = [...filtered];
            } else {
                this.filteredItems = [];
            }
        }
    }

    sBLoaded(args) {
        const searchbar: SearchBar = <SearchBar>args.object;
        if (searchbar.android) {
            searchbar.android.clearFocus();
        }
    }

    onNotificationTap(args) {
        this.notificationsService.currentNotification = args.view.bindingContext;
        this.router.navigate(['/notification-detail']);
    }
}
