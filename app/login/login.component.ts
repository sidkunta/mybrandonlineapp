import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import * as ApplicationSettings from 'application-settings';
import * as dialogs from 'ui/dialogs';
import {
    // FormBuilder,
    FormGroup
    // Validators
} from '@angular/forms';
import { AuthService } from '../services/auth.services';
// import { IUserLogin } from '../models/interfaces';
import {
    Feedback
    // FeedbackType,
    // FeedbackPosition
} from 'nativescript-feedback';
// import { Page } from 'tns-core-modules/ui/page';

@Component({
    moduleId: module.id,
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
    loginForm: FormGroup;
    errorMessage: string;
    public input: any;
    serverSwapCounter = 0;
    isAgent = false;

    constructor(
        private router: RouterExtensions,
        private authService: AuthService,
        private feedback: Feedback
    )  {
        // this.input = {
        //     Username: 'customer1@mybrand.com',
        //     Password: 'wca_2018'
        // };
        this.input = {
            Username: 'musercv@cv.com',
            Password: '12345678'
        };
        this.serverSwapCounter = 0;
    }
    @ViewChild("inputField") usernameElem: ElementRef;
    ngOnInit() {
        console.log('Login page initiated');
        console.log(
            'Authenticated ' + ApplicationSettings.getBoolean('authenticated')
        );
        // if (ApplicationSettings.getBoolean('authenticated', false)) {
        //     this.router.navigate(['/dashboard'], { clearHistory: true });
        // }
    }

    ngAfterViewInit() {
        // TODO: remove this
        // this.login();
    }

    onTapCustomer() {
        this.isAgent = false;
    }

    onTapAgent() {
        this.isAgent = true;
    }

    login() {
        ApplicationSettings.setBoolean('isAgent', this.isAgent);
        if (
            this.input.Username.trim() !== '' &&
            this.input.Password.trim() !== ''
        ) {
            this.authService.login(this.input, this.isAgent).subscribe(
                () => {
                    // console.log(response);
                    this.router.navigate(['/dashboard']);
                },
                () => {
                    this.feedback.show({
                        message: 'Invalid credentials!'
                    });
                    // console.log(error);
                }
            );
        } else {
            this.feedback.show({
                message: 'All fields required!'
            });
        }
    }

    onTapIcon() {
        this.serverSwapCounter++;

        if (this.serverSwapCounter === 5) {
            this.serverSwapCounter = 0;

            dialogs.action('Select a server environment', 'Cancel', [
                'signalr',
                'dev',
                'dev2',
                'qat',
                'qat2',
                'cvt',
                'integration',
                'win',
                'prod'
            ]).then((value) => {
                if (value !== 'Cancel') {
                    ApplicationSettings.setString('serverEnv', value);

                    this.feedback.show({
                        message: `Server switched to ${value}`
                    });
                }
            });
        }
    }
    public onButtonTap() {
        this.usernameElem.nativeElement.focus();
    }
}
